import { message } from "antd";
import React, { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";
import { axiosInstance } from "../helpers/axiosInstance";

function Payment() {
 const Params = useParams();
  const [flight, setFlight] = useState();
  const getFlight= async () => {
    try {
      const response = await axiosInstance.post("/api/flights/get-flight-by-id", {
        _id: Params.id,
      });
      if (response.data.success) {
        setFlight(response.data.data);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      message.error(error.message);
    }
  };
  useEffect(() => {
    getFlight();
  },[] );
  return (
   <div>
           { (
              <h1 className="text-2xl primary-text">payment done</h1>
           )}
            <p className="text-md">
             
              </p>
              <p className="text-md">
                
              </p>

    </div>
          
     
  )
  }
export default Payment;
